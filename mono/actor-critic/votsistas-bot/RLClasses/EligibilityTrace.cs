﻿// -----------------------------------------------------------------------
// <copyright file="EligibilityTrace.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.RLClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public class EligibilityTrace
    {
        public Observation observation { get; set; }
        public PlayerAction action { get; set; }
        public double value { get; set; }

        public EligibilityTrace() { }

        public EligibilityTrace(Observation o, PlayerAction a, double v)
        {
            this.observation = o;
            this.action = a;
            this.value = v;
        }
    }
}
