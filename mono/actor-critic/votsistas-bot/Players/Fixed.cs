﻿// -----------------------------------------------------------------------
// <copyright file="Fixed.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Players
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using votsistas_bot.Messages.Bot;
    using votsistas_bot.RLClasses;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>

    [Serializable]
    public class Fixed : BasePlayer
    {
        public Fixed() { }
        public Fixed(string name)
            : base(name)
        {       }
        public PlayerAction getAction()
        {
            PlayerAction action = new PlayerAction();
            return action;
        }

        protected override void takeAction(List<Messages.Server.Classes.CurrentPositions> cp)
        {
            send(new Throttle(0.5));
        }

        protected override void player_start()
        {
            base.player_start();
        }

        protected override void player_end(double millis)
        {
            base.player_end(millis);
        }

        protected override void player_cleanup()
        {
            base.player_cleanup();
        }

        protected override void checkLapTime(double millis)
        {
            base.checkLapTime(millis);
        }
    }
}
