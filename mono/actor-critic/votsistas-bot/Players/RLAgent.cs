﻿// -----------------------------------------------------------------------
// <copyright file="RLAgent.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Players
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using NeuronDotNet.Core.Backpropagation;
    using NeuronDotNet;
    using NeuronDotNet.Core.Initializers;
    using NeuronDotNet.Core;
    using System.Xml.Serialization;
    using System.Xml;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;
    using votsistas_bot.CommunicationHandlers;
    using votsistas_bot.Messages.Bot;
    using System.Diagnostics;
    using votsistas_bot.Messages.Server.Classes;
    using votsistas_bot.RLClasses;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    /// 

    [Serializable]
    public class RLAgent : BasePlayer
    {

        #region Fields

        private double bestLap = 0;

        protected double[] actions = new double[] { 0, 0.2, 0.4, 0.6, 0.8, 1 };

        public bool pendingRightLaneChange = false;
        public bool pendingLeftLaneChange = false;
        public bool pendingLineChange = false;

        public bool canSwitchRight = false;
        public bool shouldSwitchRight = false;
        public bool shouldSwitchLeft = false;
        public bool canSwitchLeft = false;

        public bool opponentNear = false;
        public bool wonGame = false;

        //Last observation
        public Observation lastState { get; set; }
        public double lastAction { get; set; }

        //Traces
        public List<EligibilityTrace> traces = new List<EligibilityTrace>();

        //Neural Network
        NeuronDotNet.Core.Network network;

        //Current epoch - used only for training the nn
        public int currentEpoch;

        //RL - parameters
        public double epsilon { get; set; }
        public double alpha { get; set; }
        public double gamma { get; set; }
        public double lamda { get; set; }

        public bool policyFrozen { get; set; }
        public bool isAlive { get; set; }

        #endregion Fields

        #region Override functions

        public RLAgent(string name)
            : base(name)
        {
            agent_init(false, name, 12);
        }

        protected override void player_start()
        {
            base.player_start();

            agent_start();
        }

        protected override void takeAction(List<Messages.Server.Classes.CurrentPositions> cp)
        {
            double action;

            if (cp.Count == 0)//crashed
            {
                action = agent_step(this.lastState, getReward());
            }
            else
            {
                action = agent_step(getObservation(cp), getReward());
            }

            if (checkTurbo())
                return;

            if (!pendingLineChange)
            {
                if (canSwitchRight && shouldSwitchRight && (!pendingRightLaneChange))
                {
                    pendingLeftLaneChange = false;
                    pendingRightLaneChange = true;
                    send(new votsistas_bot.Messages.Bot.Switch("Right"));
                    Console.WriteLine("Switching to right lane..!");
                }
                else if (canSwitchLeft && shouldSwitchLeft && (!pendingLeftLaneChange))
                {
                    pendingLeftLaneChange = true;
                    pendingRightLaneChange = false;
                    send(new votsistas_bot.Messages.Bot.Switch("Left"));
                    Console.WriteLine("Switching to left lane..!");
                }
                else
                {
                    send(new Throttle(Math.Abs(action)));
                    Console.WriteLine("Throttle value : " + Math.Abs(action));
                }
            }
            else
            {
                send(new Throttle(Math.Abs(action)));
                Console.WriteLine("Throttle value : " + Math.Abs(action));
            }

            updateCurrentLaneInfo(cp.First(x => x.id.name == this.playerID.name));
            this.previousPosition = cp.First(x => x.id.name == this.playerID.name).piecePosition;
        }

        protected override void player_end(double millis)
        {
            base.player_end(millis);

            // wonGame = isWinner;

            //if (wonGame)
            //    agent_end(10);
            //else
            //    agent_end(-10);

            agent_end(-Math.Abs(millis));
        }

        protected override void player_cleanup()
        {
            base.player_cleanup();

            agent_cleanup();
        }

        protected override void checkLapTime(double millis)
        {
            agent_step(this.lastState, -Math.Abs(millis));
        }

        #endregion

        #region RLMethods

        //Initialize agent's parameters
        public void agent_init(bool policy, string agentName, int inputCount)
        {
            //Initialize neural net
            LinearLayer inputLayer = new LinearLayer(inputCount + 1);
            SigmoidLayer hiddenLayer = new SigmoidLayer(30);
            LinearLayer outputLayer = new LinearLayer(1);

            new BackpropagationConnector(inputLayer, hiddenLayer).Initializer = new RandomFunction(-0.5, 0.5);
            new BackpropagationConnector(hiddenLayer, outputLayer).Initializer = new RandomFunction(-0.5, 0.5);

            this.network = new BackpropagationNetwork(inputLayer, outputLayer);
            Console.WriteLine("Neural initialized");
            this.network.SetLearningRate(0.3);
            this.network.Initialize();

            #region Initialize_parameters

            this.policyFrozen = policy;

            if (policy)
            {
                this.epsilon = 0;
                this.alpha = 0;
            }
            else
            {
                this.epsilon = 0.5;
                this.alpha = 0.2;
            }


            this.gamma = 0.95;
            this.lamda = 0.8;

            currentEpoch = 1;

            #endregion Initialize_parameters
        }

        //First action of the agent, where no reward is to be expected from the environment
        public double agent_start()
        {
            //Increase currentEpoch paramater ( used only in nn training)
            currentEpoch++;

            //Initialize agent's parameters
            initParams();

            return 1;

            /*
            //Create new array for action
            double action = 0;

            ///Calculate Qvalues
            double[] QValues = calculateQValues(observation);

            //Select final action based on the ε-greedy algorithm
            action = e_greedySelection(QValues);

            //Update local values
            lastAction = action;
            lastState = observation;

            traces.Add(new EligibilityTrace(observation, new votsistas_bot.RLClasses.PlayerAction(action), 1));

            return action;
             * */
        }

        //Receive an observation and a reward from the environment and send the appropriate action
        public double agent_step(Observation observation, double reward)
        {
            //If this isn't a random agent calculate the Q values for every possible action
            double action = 0;
            //Calculate Qvalues
            double[] QValues = calculateQValues(observation);
            //Select action
            action = e_greedySelection(QValues);
            //If the policy of the agent isn't frozen then train the neural network
            if (!policyFrozen)
            {
                //If the agent is learning then update it's qValue for the selected action
                double QValue = 0;
                bool exists = false;
                //Calculate the qValues using the Q-learning
                exists = updateQTraces(observation, new PlayerAction(action), reward);
                QValue = Qlearning(lastState, new PlayerAction(lastAction), observation, new PlayerAction(findMaxValues(QValues)), reward);

                trainNeural(createInput(lastState, lastAction), QValue);

                //Add trace to list
                if (!exists)
                    traces.Add(new EligibilityTrace(lastState, new PlayerAction(lastAction), 1));

            }

            //Update local values
            lastAction = action;
            lastState = observation;
            return action;
        }

        //End of current game
        public void agent_end(double reward)
        {
            //Mark this agent as dead
            this.isAlive = false;

            updateQTraces(lastState, new PlayerAction(lastAction), reward);

            //Reduce RL-parameters values
            epsilon *= 0.99;
            alpha *= 0.99;
        }

        //Occurs when the experiment ( total set of games ) is finished
        public void agent_cleanup()
        {
            //If this isn't a random agent and hasn't a frozen policy then store the agent to a file
            if (!this.policyFrozen)
            {
                if (!Directory.Exists(backupPath))
                    Directory.CreateDirectory(backupPath);

                using (FileStream fs = new FileStream(backupPath + @"\nn-" + this.playerID.name + ".dat", FileMode.Create))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, this.network);
                    // formatter.Serialize(fs, this);
                }

                using (FileStream fs = new FileStream(backupPath + @"\" + this.playerID.name + ".dat", FileMode.Create))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    //formatter.Serialize(fs, this.network);
                    formatter.Serialize(fs, this);
                }

                Console.WriteLine("Saving player's " + this.playerID.name + " network...");
            }
        }

        public void playVirtualGames(string games)
        {
            int curGame = Int32.Parse(games);
            currentEpoch = curGame;
            for (int i = 1; i < curGame; i++)
            {
                //Reduce RL-parameters values
                epsilon *= 0.99;
                alpha *= 0.99;
            }
        }

        //Change agent's current observation based on what the agent receives
        public void agent_changeCurrentState(Observation obs)
        {
            this.lastState = obs;
        }

        #endregion RLMethods

        #region MiscMethods

        //Initialize local parameters for a new game
        public void initParams()
        {
            if (this.policyFrozen)
            {
                this.alpha = 0;
                this.epsilon = 0;
                this.lamda = 0;
                this.gamma = 0;
            }

            this.agent_changeCurrentState(new Observation());
            //this.wonGame = false;
            this.isAlive = true;
            lastAction = 0;
            lastState = new Observation();

            bestLap = 0;
            pendingLineChange = false;
            pendingRightLaneChange = false;
            pendingLeftLaneChange = false;
            canSwitchLeft = true;
            canSwitchRight = true;

            traces = new List<EligibilityTrace>();
        }

        public Network getNeural()
        {
            return this.network;
        }

        //Set agent's neural network
        public void setNeural(Network net)
        {
            this.network = net;
            Console.WriteLine("Neural set");
        }

        public void loadNeural()
        {
            FileStream fs = new FileStream(backupPath + @"\nn-" + this.playerID.name + ".dat", FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();
            this.network = (Network)formatter.Deserialize(fs);
            Console.WriteLine("Neural loaded from " + backupPath);
            fs.Close();
        }

        //Train agent's neural network for specific input and desired output
        private void trainNeural(double[] input, double output)
        {
            double[] tmp = { output };

            //Create the training sample for the neural network 
            TrainingSample sample = new TrainingSample(input, tmp);

            //Train nn
            network.Learn(sample, 0, currentEpoch);

        }

        //Create input for the neural networkcreate
        public double[] createInput(Observation state, double action)
        {
            List<double> input = new List<double>();

            // input.Add(state.position);
            input.Add(state.currentSpeed);
            //  input.Add(state.distance);
            for (int i = 0; i < state.angle.Length; i++)
            {
                input.Add(state.angle[i]);
                input.Add(state.length[i]);
            }
            input.Add(state.collisionProbability);
            input.Add(state.currentAngle);
            input.Add(state.leftLaneInfo);
            input.Add(state.rightLaneInfo);
            input.Add(state.currentLaneInfo);
            input.Add(action);

            //Return the input array
            return input.ToArray();
        }

        //ε-greedy Selection Algorithm
        private double e_greedySelection(double[] QValues)
        {
            //Create new action array
            double actionSelected = 0;

            //Calculate a new random value between 0-1
            Random rnd = new Random();
            double val = rnd.NextDouble();

            //Based on that value select either a random action or the best possible
            if (val >= this.epsilon)
            {
                //Select best action
                actionSelected = findMaxValues(QValues);
            }
            else
            {
                //Select random action
                actionSelected = randomAction();
            }

            return actionSelected;
        }

        //Calculate network's output
        private double[] calculateQValues(Observation obs)
        {
            double[] tempQ = new double[actions.Length];
            for (int i = 0; i < actions.Length; i++)
            {
                //Run netowrk for action i,j to given observation
                double[] input = createInput(obs, actions[i]);
                tempQ[i] = network.Run(input)[0];
            }
            return tempQ;
        }

        //Return the best value of a 2-d array. Ties are being broken randomly
        private double findMaxValues(double[] tempQ)
        {
            //Create a new action and set the first qValue as max
            double selectedValue = -1;
            double maxValue = tempQ[0];

            //Search through the whole Q array to find the maximum value
            for (int i = 0; i < tempQ.Length; i++)
            {
                if (tempQ[i] > maxValue)
                {
                    selectedValue = actions[i];
                    maxValue = tempQ[i];
                }
                //Break ties randomly
                else if (tempQ[i].Equals(maxValue))
                {
                    Random rnd = new Random();
                    double prValue = rnd.NextDouble();
                    double curValue = rnd.NextDouble();
                    if (curValue > prValue)
                    {
                        selectedValue = actions[i];
                        maxValue = tempQ[i];
                    }
                }
            }
            return selectedValue;
        }

        //Q learning algorithm
        private double Qlearning(Observation p_lastState, votsistas_bot.RLClasses.PlayerAction p_lastAction, Observation newState, votsistas_bot.RLClasses.PlayerAction bestAction, double reward)
        {
            double QValue = network.Run(createInput(p_lastState, p_lastAction.throttle)).First();

            //run network for last state and last action
            double previousQ = QValue;

            //run network for new state and best action
            double newQ = network.Run(createInput(newState, bestAction.throttle)).First();

            QValue += alpha * (reward + gamma * newQ - previousQ);

            return QValue;
        }

        //Update traces -- qlearning---Peng's Q(λ)
        private bool updateQTraces(Observation obs, votsistas_bot.RLClasses.PlayerAction a, double reward)
        {
            bool found = false;

            //Since the state space is huge we'll use a similarity function to decide whether two states are similar enough
            for (int i = 0; i < traces.Count; i++)
            {
                if (traces[i].observation == obs && (!a.throttle.Equals(traces[i].action.throttle)) || traces[i].value < Math.Pow(10, -3))
                {
                    traces[i].value = 0;
                    traces.RemoveAt(i);
                    i--;

                }
                else if (traces[i].observation == obs && (a.throttle.Equals(traces[i].action.throttle)))
                {
                    found = true;

                    traces[i].value = 1;

                    //Q[t] (s,a)
                    double qT = network.Run(createInput(traces[i].observation, traces[i].action.throttle))[0];

                    //maxQ[t] (s[t+1],a) 
                    double act = findMaxValues(calculateQValues(obs));
                    double maxQt = network.Run(createInput(obs, act))[0];

                    //maxQ[t] (s[t],a)
                    act = findMaxValues(calculateQValues(lastState));
                    double maxQ = network.Run(createInput(lastState, act))[0];

                    //Q[t+1] (s,a) = Q[t] (s,a) + alpha * ( trace[i].value ) * ( reward + gamma * maxQ[t] (s[t+1],a) * maxQ[t] (s[t],a))
                    double qVal = qT + alpha * (traces[i].value) * (reward + gamma * maxQt - maxQ);

                    trainNeural(createInput(traces[i].observation, traces[i].action.throttle), qVal);

                }
                else
                {
                    traces[i].value = gamma * lamda * traces[i].value;

                    //Q[t] (s,a)
                    double qT = network.Run(createInput(traces[i].observation, traces[i].action.throttle))[0];

                    //maxQ[t] (s[t+1],a) 
                    double act = findMaxValues(calculateQValues(obs));
                    double maxQt = network.Run(createInput(obs, act))[0];

                    //maxQ[t] (s[t],a)
                    act = findMaxValues(calculateQValues(lastState));
                    double maxQ = network.Run(createInput(lastState, act))[0];

                    //Q[t+1] (s,a) = Q[t] (s,a) + alpha * ( trace[i].value ) * ( reward + gamma * maxQ[t] (s[t+1],a) * maxQ[t] (s[t],a))
                    double qVal = qT + alpha * (traces[i].value) * (reward + gamma * maxQt - maxQ);

                    trainNeural(createInput(traces[i].observation, traces[i].action.throttle), qVal);
                }
            }

            return found;
        }

        private void updateCurrentLaneInfo(CurrentPositions cp)
        {
            if (this.race.track.pieces[cp.piecePosition.pieceIndex].@switch == true)
            {
                if (cp.piecePosition.lane.endLaneIndex != cp.piecePosition.lane.startLaneIndex)
                {
                    pendingLineChange = true;
                    pendingRightLaneChange = false;
                    pendingLeftLaneChange = false;
                }
                else
                {
                    pendingLineChange = false;
                }
            }

            // Should switch lanes in order to take a "close" turn
            // Move forward from opponent
            double angle = this.race.track.pieces.Skip(cp.piecePosition.pieceIndex).ToList().First(x => x.angle != 0).angle;
            if (angle > 0)
            {
                shouldSwitchRight = true;
                shouldSwitchLeft = false;
            }
            else
            {
                shouldSwitchRight = false;
                shouldSwitchLeft = true;
            }
        }

        #region Action methods

        //Return a random action for the
        private double randomAction()
        {
            Random rnd = new Random();
            // if (rnd.Next(1, 5000) > 2500)
            return actions[rnd.Next(0, actions.Length - 1)];
            // else
            //     return (rnd.NextDouble() * (-1));

        }

        private double getReward()
        {

            //if (wonGame)
            //    return 10;
            //else
            //{
            //    if (bestLapRewardActive)
            //    {
            //        bestLapRewardActive = false;
            //        return 5;
            //    }
            return 0;
            //}
        }

        private bool checkTurbo()
        {
            if (isTurboAvailable)
            {
                //Activate turbo when : 
                //Straight line ahead of me
                bool shouldUseTurbo = true;
                bool turnNear = false;
                for (int i = 0; i < 5; i++)
                {
                    if (this.race.track.pieces[i + this.previousPosition.pieceIndex].angle != 0)
                    {
                        if (i < 2)
                            turnNear = true;
                        shouldUseTurbo = false;
                    }
                }

                //Opponent near me and approaching turn
                if (!shouldUseTurbo)
                {
                    if (turnNear)
                    {
                        if (opponentNear)
                            shouldUseTurbo = true;
                    }
                }


                if (shouldUseTurbo)
                {
                    send(new Turbo("Lets get theeeeem!"));
                    Console.WriteLine("Turbo sent..!");
                    isTurboAvailable = false;

                    return true;
                }
            }

            return false;
        }


        #endregion

        #region Observation methods

        private Observation getObservation(List<Messages.Server.Classes.CurrentPositions> cp)
        {
            Observation state = new Observation();

            // state.position = getPositionPercentage(getPlayersOrder(cp));
            state.currentSpeed = calculateSpeed(cp.First(x => x.id.name == this.playerID.name).piecePosition) / 10;
            // state.distance = getDistanceFromFirst(cp);
            state.angle = getAngles(cp.First(x => x.id.name == this.playerID.name).piecePosition.pieceIndex);
            state.length = getLengths(cp.First(x => x.id.name == this.playerID.name).piecePosition.pieceIndex);
            state.collisionProbability = getCollisionProbability(state.currentSpeed, cp);
            state.currentAngle = Math.Sin(cp.First(x => x.id.name == this.playerID.name).angle); //car's angle
            state.currentLaneInfo = getCurrentLaneInfo(cp);
            state.leftLaneInfo = getLeftLaneInfo(cp);
            state.rightLaneInfo = getRightLaneInfo(cp);

            return state;
        }

        #region Position

        private double getPositionPercentage(List<string> order)
        {
            if (order.Count == 1)
                return 1;

            for (int i = 0; i < order.Count - 1; i++)
            {
                if (order[i] == this.playerID.name)
                    return i / order.Count;
            }

            return 1;
        }

        private List<string> getPlayersOrder(List<Messages.Server.Classes.CurrentPositions> cp)
        {
            List<string> order = cp.OrderBy(x => x.piecePosition.lap).ThenBy(x => x.piecePosition.pieceIndex).ThenBy(x => x.piecePosition.inPieceDistance).Select(x => x.id.name).ToList<String>();
            return order;
        }

        #endregion

        #region Current Speed

        private double calculateSpeed(PiecePosition pp)
        {
            if (this.previousPosition.pieceIndex == pp.pieceIndex)
            {
                return pp.inPieceDistance - this.previousPosition.inPieceDistance;
            }
            else
            {
                if (this.race.track.pieces[this.previousPosition.pieceIndex].radius > 0) //  ( angle/360 ) * 2 * π * radius
                    return
                        (((this.race.track.pieces[this.previousPosition.pieceIndex].angle / 360) * 2 * Math.PI * this.race.track.pieces[this.previousPosition.pieceIndex].radius)
                                        - this.previousPosition.inPieceDistance) + pp.inPieceDistance;
                else
                    return ((this.race.track.pieces[this.previousPosition.pieceIndex].length - this.previousPosition.inPieceDistance) + pp.inPieceDistance);
            }
        }

        #endregion

        #region Distance from first

        protected double getDistanceFromFirst(List<CurrentPositions> cp)
        {
            double distance = 0;

            CurrentPositions first = cp.OrderBy(x => x.piecePosition.lap).ThenBy(x => x.piecePosition.pieceIndex).ThenBy(x => x.piecePosition.inPieceDistance).First();

            if (first.id.name == this.playerID.name)
                return 0;

            else
            {
                if (first.piecePosition.lap == cp.First(x => x.id.name == this.playerID.name).piecePosition.lap)
                    distance = (first.piecePosition.pieceIndex - cp.First(x => x.id.name == this.playerID.name).piecePosition.pieceIndex) / this.race.track.pieces.Count;
                else
                {
                    distance = (int)(first.piecePosition.lap - cp.First(x => x.id.name == this.playerID.name).piecePosition.lap);
                }
            }

            return distance;
        }

        #endregion

        #region Angle

        private double[] getAngles(int pieceIndex)
        {
            double[] angles = new double[3];

            for (int i = 0; i < 3; i++)
            {
                angles[i] = Math.Sin(this.race.track.pieces[(i + pieceIndex) % this.race.track.pieces.Count].angle);
            }
            return angles;
        }

        #endregion

        #region Length

        private double[] getLengths(int pieceIndex)
        {
            double[] lengths = new double[3];

            for (int i = 0; i < 3; i++)
            {
                if (this.race.track.pieces[(i + pieceIndex) % this.race.track.pieces.Count].angle > 0)
                    lengths[i] = ((this.race.track.pieces[(i + pieceIndex) % this.race.track.pieces.Count].angle / 360) * 2 * Math.PI * this.race.track.pieces[(i + pieceIndex) % this.race.track.pieces.Count].radius);
                else
                    lengths[i] = this.race.track.pieces[(i + pieceIndex) % this.race.track.pieces.Count].length;
            }
            return lengths;
        }

        #endregion

        #region Collision Probability

        private double getCollisionProbability(double currentSpeed, List<CurrentPositions> cp)
        {
            bool found = false;
            double collsionProbabilty = 0;

            //Pithanotita proskrousis sta epomena 5 kommatia 
            // -10% gia kathe kommati makria  diladi
            //                       an einai vevaio pws tha trakarw se 5 kommatia tote 1 - 0.5
            //                                                       se 4 kommatia tote 1 - 0.4

            //(Abs) angle of turn - angle of car 

            double radius = 0;
            double angle = 0;

            int dist = 0;
            double distanceFromTurn = 0;

            int currentPieceIndex = cp.First(x => x.id.name == this.playerID.name).piecePosition.pieceIndex;
            for (int i = 0; i < 3; i++)
            {
                if (this.race.track.pieces[(currentPieceIndex + i) % this.race.track.pieces.Count].angle > 0)
                {
                    radius = this.race.track.pieces[(currentPieceIndex + i) % this.race.track.pieces.Count].radius;
                    angle = Math.Abs(this.race.track.pieces[(currentPieceIndex + i) % this.race.track.pieces.Count].angle);
                    dist = i;
                    found = true;
                    break;
                }
            }

            if (!found)
                collsionProbabilty = 0;
            else
            {
                distanceFromTurn = 1 - ((double)dist / 10);
                double maximumSpeed = Math.Sqrt(Math.Abs(Math.Tan(angle) * radius * 10)) / 100;

                //if (currentSpeed / maximumSpeed > 0)
                collsionProbabilty = (currentSpeed / maximumSpeed);// - distanceFromTurn;
                //else
                //    collsionProbabilty = 1 - distanceFromTurn;
            }

            return collsionProbabilty;
        }

        #endregion

        #region Lanes Info

        protected double getCurrentLaneInfo(List<CurrentPositions> cp)
        {
            opponentNear = false;

            //If opponent in right lane before switch then -1 if no opponent 1 if opponent in left before switch 0
            //Get next switch 
            // -1 episis an den mporei na stripsei!
            int currentPieceIndex = cp.First(x => x.id.name == this.playerID.name).piecePosition.pieceIndex;
            int playerslane = cp.First(x => x.id.name == this.playerID.name).piecePosition.lane.endLaneIndex;
            int nextSwitchIndex = this.race.track.pieces.Skip(currentPieceIndex).ToList<Piece>().FindIndex(x => x.@switch == true);

            foreach (CurrentPositions c in cp)
            {
                if (c.id.name != this.playerID.name)
                {
                    if (c.piecePosition.pieceIndex >= currentPieceIndex && c.piecePosition.pieceIndex <= nextSwitchIndex)
                    {
                        if (c.piecePosition.lane.endLaneIndex == playerslane)
                        {
                            shouldSwitchLeft = true;
                            shouldSwitchRight = true;

                            if (c.piecePosition.pieceIndex < this.previousPosition.pieceIndex + 2)
                                opponentNear = true;

                            return -1;
                        }
                    }
                }
            }

            return 1;
        }

        protected double getRightLaneInfo(List<CurrentPositions> cp)
        {
            //If opponent in right lane before switch then -1 if no opponent 1 if opponent in left before switch 0
            //Get next switch 
            // -1 episis an den mporei na stripsei!
            int currentPieceIndex = cp.First(x => x.id.name == this.playerID.name).piecePosition.pieceIndex;
            int playerslane = cp.First(x => x.id.name == this.playerID.name).piecePosition.lane.endLaneIndex;
            int nextSwitchIndex = this.race.track.pieces.Skip(currentPieceIndex).ToList<Piece>().FindIndex(x => x.@switch == true);

            bool inLeftLane = false;

            foreach (CurrentPositions c in cp)
            {
                if (c.id.name != this.playerID.name)
                {
                    if (c.piecePosition.pieceIndex >= currentPieceIndex && c.piecePosition.pieceIndex <= nextSwitchIndex)
                    {
                        if (c.piecePosition.lane.endLaneIndex == playerslane + 1)
                            return -1;
                        else if (c.piecePosition.lane.endLaneIndex == playerslane - 1)
                            inLeftLane = true;
                    }
                }
            }

            if (this.race.track.lanes.OrderBy(x => x.index).Last().index == playerslane)
            {
                canSwitchRight = false;
                return -1;
            }
            else
                canSwitchRight = true;

            if (inLeftLane)
                return 0;

            return 1;
        }

        protected double getLeftLaneInfo(List<CurrentPositions> cp)
        {
            //If opponent in right lane before switch then -1 if no opponent 1 if opponent in left before switch 0
            //Get next switch 
            // -1 episis an den mporei na stripsei!
            int currentPieceIndex = cp.First(x => x.id.name == this.playerID.name).piecePosition.pieceIndex;
            int playerslane = cp.First(x => x.id.name == this.playerID.name).piecePosition.lane.endLaneIndex;
            int nextSwitchIndex = this.race.track.pieces.Skip(currentPieceIndex).ToList<Piece>().FindIndex(x => x.@switch == true);

            bool inRightLane = false;

            foreach (CurrentPositions c in cp)
            {
                if (c.id.name != this.playerID.name)
                {
                    if (c.piecePosition.pieceIndex >= currentPieceIndex && c.piecePosition.pieceIndex <= nextSwitchIndex)
                    {
                        if (c.piecePosition.lane.endLaneIndex == playerslane - 1)
                            return -1;
                        else if (c.piecePosition.lane.endLaneIndex == playerslane + 1)
                            inRightLane = true;
                    }
                }
            }

            if (this.race.track.lanes.OrderBy(x => x.index).First().index == playerslane)
            {
                canSwitchLeft = false;
                return -1;
            }
            else
                canSwitchLeft = true;

            if (inRightLane)
                return 0;

            return 1;
        }

        #endregion

        #endregion

        #endregion MiscMethods
    }
}