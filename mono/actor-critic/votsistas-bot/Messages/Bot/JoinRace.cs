﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace votsistas_bot.Messages.Bot
{
    class JoinRace : SendMsg
    {
        public BotId botId = new BotId();
        public string color;
        public string trackName;
        public string password;
        public int carCount;

        public JoinRace(string name, string key, string trackName, int carCount)
        {
            this.botId.name = name;
            this.botId.key = key;
            //this.color = color;
            this.trackName = trackName;
            this.carCount = carCount;
        }
        public JoinRace(string name, string key, string trackName, string password, int carCount, string color)
        {
            this.botId.name = name;
            this.botId.key = key;
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
            this.color = color;
        }


        protected override string MsgType()
        {
            return "joinRace";
        }
    }
}