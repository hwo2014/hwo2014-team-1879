﻿// -----------------------------------------------------------------------
// <copyright file="Create.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages.Bot.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Create : SendMsg
    {
        public BotId botId = new BotId();
        public string trackName;
        public string password;
        public int carCount;

        public Create(string name, string key, string trackName, int carCount)
        {
            this.botId.name = name;
            this.botId.key = key;
            this.trackName = trackName;
            this.carCount = carCount;
        }
        public Create(string name, string key, string trackName, string password, int carCount)
        {
            this.botId.name = name;
            this.botId.key = key;
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }

        protected override string MsgType()
        {
            return "create";
        }
    }
}