﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using votsistas_bot.Messages;
using votsistas_bot.Players;
using votsistas_bot.CommunicationHandlers;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

public class votsistas
{
    private static Connection cnn;

    public static void Main(string[] args)
    {
        string name = string.Empty;
        string type = string.Empty;
        string track = string.Empty;
        string numberOfPlayers = string.Empty;
        string currentGame = string.Empty;

        string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;


        #region Input Params

        try
        {
            name = args[0];
            type = args[1];
            track = args[2];
            numberOfPlayers = args[3];
            currentGame = args[4];
        }
        catch (Exception ex)
        {
            name = "dinbur";
            type = "rl";
            track = "keimola";
            numberOfPlayers = "1";
            currentGame = "1";
            //  Console.Error.WriteLine("Exception : " + ex.Message + "\nNow exiting...");
            //  Console.ReadLine();
            //  return;
        }


        if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(type) || String.IsNullOrEmpty(track) || String.IsNullOrEmpty(numberOfPlayers) || String.IsNullOrEmpty(currentGame))
        {
            Console.Error.WriteLine("Invalid input arguments. Required name and type of player.\nNow exiting...");
            Console.ReadLine();
            return;
        }

        #endregion

        Naive naivePlayer = new Naive(name);
        Fixed fixedPlayer = new Fixed(name);
        RLAgent dantibot = new RLAgent(name);

        Console.WriteLine(String.Format("Player name : {0}\nType : {1}\nTrack : {2}\nNumber of players : {3}\nCurrent game : {4}\nStarting application....", name, type, track, numberOfPlayers, currentGame));
        cnn = new Connection("senna.helloworldopen.com", 8091, name, "6AQuVM8/F5h2RQ");
        cnn.printConnInfo();

        using (TcpClient client = cnn.getClient())
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;
            //string path = String.Format(@"C:\Users\dante\Desktop\votsistas-bot\mono\votsistas-bot\votsistas-bot\Backup\nn-{0}-{1}.dat", name, DateTime.Now.ToString("yyyyMMdd"));
            //string path = String.Format(@"C:\Users\mpaipana\Desktop\votsistas-bot\mono\votsistas-bot\votsistas-bot\Backup\nn-{0}.dat", name);
            if (File.Exists(path + @"\Backup\nn-" + name + ".dat"))
            {
                dantibot.loadNeural();
                if (Int32.Parse(currentGame) > 1)
                {
                    dantibot.playVirtualGames(currentGame);
                }
            }

            if (type == "rl")
                dantibot.JoinRace(reader, writer, cnn, track, Int32.Parse(numberOfPlayers));
            // dantibot.Join(reader, writer, cnn, track);
            else if (type == "naive")
                naivePlayer.JoinRace(reader, writer, cnn, track, Int32.Parse(numberOfPlayers));
            //  naivePlayer.Join(reader, writer, cnn, track);
            else if (type == "fixed")
                fixedPlayer.JoinRace(reader, writer, cnn, track, Int32.Parse(numberOfPlayers));
            else
                fixedPlayer.JoinRace(reader, writer, cnn, track, Int32.Parse(numberOfPlayers));
        }

        Console.WriteLine(name + " finished his game. . .");

        //if (System.Diagnostics.Debugger.IsAttached)
        //  Console.ReadLine();
    }
}