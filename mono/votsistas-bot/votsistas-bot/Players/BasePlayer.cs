﻿// -----------------------------------------------------------------------
// <copyright file="BasePlayer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Newtonsoft.Json;
namespace votsistas_bot.Players
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using votsistas_bot.Messages.Bot;
    using votsistas_bot.CommunicationHandlers;
    using votsistas_bot.Messages;
    using votsistas_bot.Messages.Bot.Classes;
    using votsistas_bot.Messages.Server;
    using Newtonsoft.Json.Linq;
    using votsistas_bot.Messages.Server.Classes;
    using System.Diagnostics;
    using System.Timers;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    /// 
    [Serializable]
    public class BasePlayer
    {
        private StreamWriter _writer;
        protected PiecePosition previousPosition = new PiecePosition();
        protected Id playerID = new Id();
        protected bool isTurboAvailable = false;
        protected bool hasCrashed = false;
        protected string backupPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\Backup";
        protected string track;
        protected double fastestlap;

        private Timer _timer;

        public BasePlayer() { }
        public BasePlayer(string name)
        {
            this.playerID.name = name;
        }

        protected Race race;

        public void Join(StreamReader reader, StreamWriter writer, Connection cnn, string track)
        {
            Join join = new Join(cnn.getName(), cnn.getKey());
            this._writer = writer;
            string line;

            send(join);
            while ((line = reader.ReadLine()) != null)
            {
                //Server Responses
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                handleServerResponse(msg);
            }
        }
        public void JoinRace(StreamReader reader, StreamWriter writer, Connection cnn, string track, int numberOfPlayers)
        {
            this.track = track;
            JoinRace joinRace = new JoinRace(cnn.getName(), cnn.getKey(), track, numberOfPlayers);
            this._writer = writer;
            string line;

            _timer = new Timer();
            _timer.Interval = 10000;
            _timer.Start();
            _timer.Elapsed += _timer_Elapsed;

            send(joinRace);

            try
            {
                while ((line = reader.ReadLine()) != null)
                {
                    _timer.Stop();
                    //Server Responses
                    MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                    handleServerResponse(msg);
                    _timer.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Environment.Exit(0);
        }

        public void Create(StreamReader reader, StreamWriter writer, Connection cnn, string track)
        {
            Create create = new Create(cnn.getName(), cnn.getKey(), track, 2);
            this._writer = writer;
            string line;

            send(create);
            while ((line = reader.ReadLine()) != null)
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                handleServerResponse(msg);
            }
        }

        protected void send(SendMsg msg)
        {
            _writer.WriteLine(msg.ToJson());
        }

        private void handleServerResponse(MsgWrapper msg)
        {
            try
            {
                switch (msg.msgType.ToLower())
                {
                    case "carpositions":
                        List<JToken> jTokenList = JsonConvert.DeserializeObject<JArray>(msg.data.ToString()).ToList();
                        List<CurrentPositions> cp = new List<CurrentPositions>();
                        foreach (JToken token in jTokenList)
                        {
                            cp.Add(token.ToObject<CurrentPositions>());
                            //if (token["id"].ToObject<Id>().name == playerID.name)
                            //{
                            //    previousPosition = token["piecePosition"].ToObject<PiecePosition>();
                            //}
                        }
                        //Debug.WriteLine(msg.data.ToString());
                        //Console.WriteLine("Car positions received!");
                        if (hasCrashed)
                            send(new Ping());
                        else
                            takeAction(cp);
                        break;
                    case "crash":
                        Crash crash = JsonConvert.DeserializeObject<Crash>(msg.data.ToString());
                        Console.WriteLine("*****************\tCrash and burn!!!\t*****************");
                        if (crash.name == this.playerID.name)
                        {
                            hasCrashed = true;
                            send(new Ping());
                        }
                        else
                            send(new Ping());
                        break;
                    case "dnf":
                        DNF dnf = JsonConvert.DeserializeObject<DNF>(msg.data.ToString());
                        Console.WriteLine("*****************\tDisqualified...!\t*****************\nWhy??" + msg.data.ToString());
                        send(new Ping());
                        break;
                    case "finish":
                        Finish finish = JsonConvert.DeserializeObject<Finish>(msg.data.ToString());
                        Console.WriteLine("*****************\t" + finish.name + " finished  the race!\t*****************");
                        send(new Ping());
                        break;
                    case "gameend":
                        GameEnd gameEnd = JsonConvert.DeserializeObject<GameEnd>(msg.data.ToString());
                        Console.WriteLine("*****************\tGame ended\t*****************\n");
                        //if (gameEnd.results[0].car.name == this.playerID.name)
                        //    player_end(true);
                        //else
                        //    player_end(false);
                        player_end(gameEnd.results.First(x => x.car.name == this.playerID.name).result.millis);
                        this.fastestlap = gameEnd.bestLaps.First(x => x.car.name == this.playerID.name).result.millis;
                        Console.WriteLine(msg.data.ToString());
                        send(new Ping());
                        break;
                    case "gameinit":
                        GameInit gameInit = JsonConvert.DeserializeObject<GameInit>(msg.data.ToString());
                        this.race = gameInit.race;
                        Console.WriteLine("*****************\tGame initializing\t*****************");
                        player_start();
                        send(new Ping());
                        break;
                    case "gamestart":
                        Console.WriteLine("*****************\tGame starts!!! Bring it on!\t*****************");
                        send(new Throttle(1));
                        break;
                    case "joinrace":
                        Console.WriteLine("*****************\tRace joined\t*****************");
                        send(new Ping());
                        break;
                    case "lapfinished":
                        LapFinished lapFinished = JsonConvert.DeserializeObject<LapFinished>(msg.data.ToString());
                        Console.WriteLine("*****************\t" + lapFinished.car.name + " finished his lap\t*****************");
                        // if (lapFinished.car.name == this.playerID.name)
                        //     checkLapTime(lapFinished.lapTime.millis);
                        send(new Ping());
                        break;
                    case "spawn":
                        Spawn spawn = JsonConvert.DeserializeObject<Spawn>(msg.data.ToString());
                        Console.WriteLine("*****************\tSpawning. Lets not make that mistake again!\t*****************");
                        if (spawn.name == this.playerID.name)
                        {
                            hasCrashed = false;
                            send(new Throttle(1));
                        }
                        else
                            send(new Ping());
                        break;
                    case "tournamentend":
                        //TournamentEnd tournamentEnd = JsonConvert.DeserializeObject<TournamentEnd>(msg.data.ToString());
                        Console.WriteLine("*****************\tTournament ends!\t*****************");
                        player_cleanup();
                        send(new Ping());
                        break;
                    case "yourcar":
                        YourCar yourCar = JsonConvert.DeserializeObject<YourCar>(msg.data.ToString());
                        Console.WriteLine("*****************\tMy brand new car! It's a " + yourCar.color + "  " + yourCar.name + "\t*****************");
                        send(new Ping());
                        break;
                    case "turboavailable":
                        Console.WriteLine("*****************\tTurbo available\t*****************");
                        TurboAvailable turboAvailable = JsonConvert.DeserializeObject<TurboAvailable>(msg.data.ToString());
                        isTurboAvailable = true;
                        send(new Ping());
                        break;
                    default:
                        Console.WriteLine("*****************\tUNKNOWN MessageType " + msg.msgType + "\t*****************");
                        Console.WriteLine("Unknown : " + msg.data.ToString());
                        //if (msg.data.ToString().ToLower().Contains("turbo"))
                        //    send(new Turbo("on"));
                        send(new Ping());
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        protected virtual void checkLapTime(double millis) { }

        protected virtual void takeAction(List<CurrentPositions> cp) { }

        protected virtual void player_start() { }

        protected virtual void player_end(double millis) { }

        protected virtual void player_cleanup()
        {
            string txtpath = backupPath + @"\fastestlaps\" + track + ".txt";
            //Write fastest laps to file
            if (File.Exists(txtpath))
            {
                TextReader tr = new StreamReader(txtpath) as TextReader;
                string text = tr.ReadToEnd();

                text += (DateTime.Now.ToString("hh:mm:ss") + "\t" + track + "\t" + (fastestlap / 1000).ToString() + " seconds. \n");
                tr.Close();

                TextWriter tw = new StreamWriter(txtpath, false);
                tw.Write(text);

                tw.Close();
            }
            else
            {
                File.WriteAllText(txtpath, (DateTime.Now.ToString("hh:mm:ss") + "\t" + track + "\t" + (fastestlap / 1000).ToString() + " seconds. \n"));
            }
        }
    }
}