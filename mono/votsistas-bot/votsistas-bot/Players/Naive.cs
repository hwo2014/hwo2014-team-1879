﻿// -----------------------------------------------------------------------
// <copyright file="Random.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Players
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using votsistas_bot.Messages.Bot;
    using votsistas_bot.RLClasses;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public class Naive : BasePlayer
    {
        public Naive(string name)
            : base(name)
        { }
        public PlayerAction getAction()
        {
            votsistas_bot.RLClasses.PlayerAction randomAction = new votsistas_bot.RLClasses.PlayerAction(new Random().NextDouble());
            return randomAction;
        }

        public double getLaneSwitch()
        {
            double random = new Random().NextDouble();

            if (random > 0.5)
                return 1;
            else
                return -1;

        }

        protected override void takeAction(List<Messages.Server.Classes.CurrentPositions> cp)
        {
            double thr = this.getAction().throttle * this.getLaneSwitch();
            //if (thr < 0)
            //                send(new Switch("Right"));
            //          else
            send(new Throttle(Math.Abs(thr)));
        }

        protected override void player_start()
        {
            base.player_start();
        }

        protected override void player_end(double millis)
        {
            base.player_end(millis);
        }

        protected override void player_cleanup()
        {
            base.player_cleanup();
        }

        protected override void checkLapTime(double millis)
        {
            base.checkLapTime(millis);
        }
    }
}
