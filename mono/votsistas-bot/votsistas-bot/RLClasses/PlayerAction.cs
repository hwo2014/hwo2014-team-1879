﻿// -----------------------------------------------------------------------
// <copyright file="Action.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.RLClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public class PlayerAction
    {
        //Int array to specify the actions that need to be taken
        public double throttle;

        //Constructor of class
        public PlayerAction()
        { }

        public PlayerAction(double t)
        {
            this.throttle = t;
        }

        public double getThrottle() { return this.throttle; }
    }
}
