﻿// -----------------------------------------------------------------------
// <copyright file="Observation.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.RLClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public class Observation
    {
        #region Featues

        //  public double position; // % of opponents currently ahead of me
        public double currentSpeed; // since every action is taken in a discrete timestep then speed(t) = position(t) - position(t-1)
        //  public double distance; // distance from best opponent
        public double[] angle = new double[3]; // angle of next 5 pieces
        public double[] length = new double[3]; // length of next 5 pieces
        public double collisionProbability; // collisionProbability driving with this speed
        public double currentAngle; // angle of car
        public double leftLaneInfo; // see notes - will probably have to change this later
        public double rightLaneInfo; // see notes - will probably have to change this later
        public double currentLaneInfo; // see notes - will probably have to change this later

        #endregion

        public Observation() { }

    }
}
