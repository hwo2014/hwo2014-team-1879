﻿// -----------------------------------------------------------------------
// <copyright file="Connection.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.CommunicationHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Net.Sockets;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Connection
    {
        string _host { get; set; }
        int _port { get; set; }
        string _botName { get; set; }
        string _botKey { get; set; }

        public Connection() { }
        public Connection(string host, int port, string botName, string botKey)
        {
            _host = host;
            _port = port;
            _botName = botName;
            _botKey = botKey;
        }

        public TcpClient getClient() { return new TcpClient(this._host, this._port); }

        public void printConnInfo()
        {
            Console.WriteLine("Connecting to " + _host + ":" + _port + " as " + _botName + "/" + _botKey);
        }

        public string getHost() { return _host; }
        public int getPort() { return _port; }
        public string getName() { return _botName; }
        public string getKey() { return _botKey; }
    }
}
