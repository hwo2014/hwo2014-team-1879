﻿// -----------------------------------------------------------------------
// <copyright file="PiecePosition.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages.Server.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    
    [Serializable]
    public class PiecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public CarLane lane;
        public int lap;
    }
}
