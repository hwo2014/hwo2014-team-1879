﻿// -----------------------------------------------------------------------
// <copyright file="Track.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages.Server.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Track
    {
        public string id;
        public string name;
        public List<Piece> pieces;
        public List<Lane> lanes;
        public Point startingPoint;

        public Track() { }
    }
}
