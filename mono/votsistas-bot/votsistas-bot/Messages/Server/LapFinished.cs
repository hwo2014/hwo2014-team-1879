﻿// -----------------------------------------------------------------------
// <copyright file="LapFinished.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages.Server
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using votsistas_bot.Messages.Server.Classes;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class LapFinished : GetMsg
    {
        public Id car;
        public LapTime lapTime;
        public Result raceTime;
        public Ranking ranking;
    }
}
