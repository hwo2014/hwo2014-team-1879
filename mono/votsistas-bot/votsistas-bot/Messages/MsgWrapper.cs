﻿// -----------------------------------------------------------------------
// <copyright file="MsgWrapper.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>

    public class MsgWrapper
    {
        public string msgType;
        public Object data;
        //public string gameId;
        //public int gameTick;

        public MsgWrapper() { }

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }

        //public MsgWrapper(string msgType, Object data, string gameId, int gameTick)
        //{
        //    this.msgType = msgType;
        //    this.data = data;
        //    this.gameId = gameId;
        //    this.gameTick = gameTick;
        //}
    }
}