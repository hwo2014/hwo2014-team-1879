﻿// -----------------------------------------------------------------------
// <copyright file="Throttle.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages.Bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Throttle : SendMsg
    {
        public double throttle;

        public Throttle(double value)
        {
            this.throttle = Math.Abs(value);
        }

        protected override Object MsgData()
        {
            return this.throttle;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }
}