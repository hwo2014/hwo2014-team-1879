﻿// -----------------------------------------------------------------------
// <copyright file="Switch.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages.Bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Switch : SendMsg
    {
        public string lane;

        public Switch(string value)
        {
            this.lane = value;
        }

        protected override Object MsgData()
        {
            return this.lane;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}