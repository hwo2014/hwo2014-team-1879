﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace votsistas_bot.Messages.Bot
{
    [Serializable]
    public class BotId
    {
        public string name;
        public string key;

        public BotId() { }
    }
}
