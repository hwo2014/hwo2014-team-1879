﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace votsistas_bot.Messages.Bot
{
    public class Turbo : SendMsg
    {
        string turbo { get; set; }

        public Turbo(string value)
        {
            this.turbo = value;
        }

        protected override Object MsgData()
        {
            return this.turbo;
        }

        protected override string MsgType()
        {
            return "turbo";
        }
    }
}