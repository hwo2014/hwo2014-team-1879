﻿// -----------------------------------------------------------------------
// <copyright file="Join.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace votsistas_bot.Messages.Bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;
        
        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
        }

        public Join(string name, string key, string color)
        {
            this.name = name;
            this.key = key;
            this.color = color;
        }

        protected override string MsgType()
        {
            return "join";
        }


    }
}