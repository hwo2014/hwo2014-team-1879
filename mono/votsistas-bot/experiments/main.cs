﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace experiments
{
    class main
    {
        static void Main(string[] args)
        {
            string[] tracks = new string[] { "usa", "keimola", "germany", "france" };

            bool p1Finisehd = false;
            //bool p2Finisehd = false;
            //bool p3Finisehd = false;
            //bool p4Finisehd = false;
            //bool p5Finisehd = false;

            int numberOfGames = 1000;

            for (int i = 0; i < numberOfGames; i++)
            {
                Console.WriteLine("\nPreparing next game. . .\n");
                Thread.Sleep(1000);

                try
                {
                    p1Finisehd = false;
                    //p2Finisehd = false;
                    //p3Finisehd = false;
                    //p4Finisehd = false;
                    //p5Finisehd = false;


                    string track = tracks[new Random().Next(0, tracks.Length)];
                    Console.WriteLine(String.Format("------------------------------------------\nCurrently on game {0} out of {1} \t Track : {2}", i + 1, numberOfGames, track));

                    //3 players
                    int numberOfPlayers = 1;

                    //Arguments for bot players
                    // 0 - name
                    // 1 - type
                    // 2 - track
                    // 3 - number of players 
                    // 4 - current game

                    BackgroundWorker bw = new BackgroundWorker();
                    bw.DoWork += new DoWorkEventHandler(delegate(object o, DoWorkEventArgs evargs)
                        {
                            LaunchCommandLineApp("dinbur", "rl", track, numberOfPlayers, i + 1);
                        });

                    //BackgroundWorker bw1 = new BackgroundWorker();
                    //bw1.DoWork += new DoWorkEventHandler(delegate(object o, DoWorkEventArgs evargs)
                    //{
                    //    LaunchCommandLineApp("frankie", "rl", track, numberOfPlayers, i + 1);
                    //});

                    //BackgroundWorker bw2 = new BackgroundWorker();
                    //bw2.DoWork += new DoWorkEventHandler(delegate(object o, DoWorkEventArgs evargs)
                    //{
                    //    LaunchCommandLineApp("goku", "rl", track, numberOfPlayers, i + 1);
                    //});

                    //BackgroundWorker bw3 = new BackgroundWorker();
                    //bw3.DoWork += new DoWorkEventHandler(delegate(object o, DoWorkEventArgs evargs)
                    //{
                    //    LaunchCommandLineApp("naive", "naive", track, numberOfPlayers, i + 1);
                    //});

                    //BackgroundWorker bw4 = new BackgroundWorker();
                    //bw4.DoWork += new DoWorkEventHandler(delegate(object o, DoWorkEventArgs evargs)
                    //{
                    //    LaunchCommandLineApp("fixed", "fixed", track, numberOfPlayers, i + 1);
                    //});

                    bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate(object o, RunWorkerCompletedEventArgs evargs) { p1Finisehd = true; });
                    //bw1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate(object o, RunWorkerCompletedEventArgs evargs) { p2Finisehd = true; });
                    //bw2.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate(object o, RunWorkerCompletedEventArgs evargs) { p3Finisehd = true; });
                    //bw3.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate(object o, RunWorkerCompletedEventArgs evargs) { p4Finisehd = true; });
                    //bw4.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate(object o, RunWorkerCompletedEventArgs evargs) { p5Finisehd = true; });

                    bw.RunWorkerAsync();

                    //Thread.Sleep(1000);
                    //bw1.RunWorkerAsync();

                    //Thread.Sleep(1000);
                    //bw2.RunWorkerAsync();

                    //Thread.Sleep(1000);
                    //bw3.RunWorkerAsync();

                    //Thread.Sleep(1000);
                    //bw4.RunWorkerAsync();

                    Console.WriteLine("All players connected... Game starting");

                    while ((!p1Finisehd))//|| (!p2Finisehd) || (!p3Finisehd) || (!p4Finisehd) || (!p5Finisehd))
                    { }

                    Console.WriteLine("Game ended...");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.WriteLine("All games have succesfully finished...\nNow exiting . . . ");
            Console.ReadLine();
        }

        /// <summary>
        /// Launch the legacy application with some options set.
        /// </summary>
        static void LaunchCommandLineApp(string name, string type, string track, int numberOfPlayers, int currentGame)
        {
            // For the example
            //const string ex1 = @"C:\Users\dante\Desktop\votsistas-bot\mono\votsistas-bot\votsistas-bot\bin\debug\votsistas-bot.exe";
            //const string ex1 = @"C:\Users\mpaipana\Desktop\votsistas-bot\mono\votsistas-bot\votsistas-bot\bin\Debug\votsistas-bot.exe";
            string ex1 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            ex1 += @"\votsistas-bot\bin\Debug\votsistas-bot.exe";

            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = true;
            startInfo.FileName = ex1;
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            startInfo.Arguments = String.Format("{0} {1} {2} {3} {4} ", name, type, track, numberOfPlayers, currentGame);

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                Process exeProcess = Process.Start(startInfo);
                exeProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                // Log error.
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}